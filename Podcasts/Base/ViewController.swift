//
//  ViewController.swift
//  Podcasts
//
//  Created by Dai Long on 11/24/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

enum StoryboardID {
    static let MaxiPlayerController = "MaxiPlayerController"
    static let MiniPlayerController = "MiniPlayerController"
}

struct ViewController {
    
    
    // MARK: - MaxiPlayer Controller
    static private let maxiPlayerStoryBoard = UIStoryboard(name: "MaxiPlayer", bundle: nil)
    
    static func maxiPlayerController() -> MaxiPlayerController {
        return maxiPlayerStoryBoard.instantiateViewController(withIdentifier: StoryboardID.MaxiPlayerController) as! MaxiPlayerController
    }
    
    static private let miniPlayerStoryBoard = UIStoryboard(name: "MiniPlayer", bundle: nil)
    static func miniPlayerController() -> MiniPlayerController {
        return miniPlayerStoryBoard.instantiateViewController(withIdentifier: StoryboardID.MiniPlayerController) as! MiniPlayerController
    }
}

//
//  DownloadsController.swift
//  Podcasts
//
//  Created by Long Vu on 5/27/19.
//  Copyright © 2019 Dai Long. All rights reserved.
//

import UIKit

class DownloadsController: UITableViewController {
    
    var episodes: [Episode] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        episodes = userDefault.downloadedEpisodes()
        self.tableView.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleProgressBar(_:)), name: .didBeginDownload, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Helper Methods
    private func setupTableView() {
        tableView.register(EpisodeCell.self)
    }
    
    @objc private func handleProgressBar(_ notification: Notification) {
        guard let userInfo = notification.userInfo as? [String: Any],
        let episode = userInfo["episode"] as? Episode,
            let progress = userInfo["progress"] as? Float else {return}
        
        let idx = episodes.firstIndex { $0.title == episode.title && $0.author == episode.author } ?? -1
        
        if idx == -1 {
            return
        }
        
        let cell: EpisodeCell = tableView.cellForRow(at: IndexPath(row: idx, section: 0))
        cell.progressView.isHidden = false
        cell.progressView.progress = progress
        if progress == 1.0 {
            cell.progressView.isHidden = true
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return episodes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: EpisodeCell = tableView.dequeueResuableCell(forIndexPath: indexPath)

        // Configure the cell...
//        cell.backgroundColor = .yellow
        cell.episode = episodes[indexPath.row]

        return cell
    }

    // MARK: - TableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let episode = self.episodes[indexPath.row]
        
        guard let mainTabBarController = tabBarController as? MainTabBarController else {
            assertionFailure("Not have tabbar")
            return
        }
        
        // global
        currentEpisode = episode
        
        mainTabBarController.miniPlayerController.episode = episode
        mainTabBarController.reCreateMaxiPlayerController(with: episode)
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            let idx = indexPath.row
            let deletedEpisode = self.episodes.remove(at: idx)
            userDefault.deleteEpisode(deletedEpisode)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}

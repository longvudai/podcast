//
//  PodcastsSearchController.swift
//  Podcasts
//
//  Created by Dai Long on 11/21/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import Alamofire

class PodcastsSearchController: UITableViewController {
    
    // MARK: - Properties
    var podcasts = [Podcast]()
    
    var timer: Timer?
    
    let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    
    let searchView: UIView = {
        let searchView = UIView(frame: CGRect.zero)
        let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
//        activityIndicatorView.style = .whiteLarge
        activityIndicatorView.color = .darkGray
        activityIndicatorView.center = searchView.center
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.startAnimating()
        
        searchView.addSubview(activityIndicatorView)
        
        return searchView
    }()
    
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupSearchController()
       
        self.setupTableView()
        
        searchView.center = self.view.center
        searchBar(searchController.searchBar, textDidChange: "NPR")
        
    }
    
    // MARK: - Setup Helper
    private func setupSearchController() {
        self.definesPresentationContext = true
        searchController.searchBar.delegate = self
        
        navigationItem.searchController = self.searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.dimsBackgroundDuringPresentation = false
    }
    
    private func setupTableView() {
        self.tableView.register(PodcastCell.self)
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.podcasts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PodcastCell = tableView.dequeueResuableCell(forIndexPath: indexPath)

        let podcast = self.podcasts[indexPath.row]
        
        cell.podcast = podcast

        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let podcast  = self.podcasts[indexPath.row]

        let episodesController = EpisodesController()
        episodesController.podcast = podcast

        navigationController?.pushViewController(episodesController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
//        let maxiPlayerStoryBoard = UIStoryboard(name: "MaxiPlayer", bundle: nil)
//
//        let maxiPlayerController = maxiPlayerStoryBoard.instantiateViewController(withIdentifier: "MaxiPlayerController") as! MaxiPlayerController//ViewController.maxiPlayerController()
//        maxiPlayerController.sourceView = self
//
//        if let tabBar = tabBarController?.tabBar {
//            maxiPlayerController.tabBarImage = tabBar.makeSnapshot()
//        }
//
//        maxiPlayerController.backingImage = UIApplication.shared.screenShot//view.makeSnapshot()
//
//        present(maxiPlayerController, animated: false)
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            let label = UILabel(frame: view.frame)
            label.text = "Please enter a Search term..."
            label.textColor = .purple
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 18.0, weight: .semibold)
            label.center = self.view.center
            
            return label
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return self.podcasts.count > 0 ? 0 : 300
    }
}

extension PodcastsSearchController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        self.podcasts.removeAll()
        self.tableView.addSubview(searchView)
        
        timer?.invalidate()
        self.activityIndicator.startAnimating()
        timer = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: { _ in
            APIService.shared.fetchPodcasts(searchText) { podcasts in
                self.podcasts = podcasts
                self.tableView.reloadData()
                self.searchView.removeFromSuperview()
            }
        })
    }
}

//extension PodcastsSearchController: MaxiPlayerSourceProtocol {
//    var originatingFrameInWindow: CGRect {
//        return view.convert(view.frame, to: nil)
//    }
//    
//    var originatingCoverImageView: UIImageView {
//        let cell: PodcastCell = tableView.dequeueReusableCell(for: IndexPath(row: 0, section: 0))
//        return cell.imageView!
//    }
//}

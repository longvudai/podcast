//
//  DraggablePresentedAnimationController.swift
//  Podcasts
//
//  Created by Dai Long on 12/12/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

class DraggablePresentedAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    var initialY: CGFloat = 0
    
    func animatePresentingInContext(_ transitionContext: UIViewControllerContextTransitioning, fromVC: UIViewController, toVC: UIViewController) {
        guard let fromVC = fromVC as? MainTabBarController else {fatalError()}
        guard let maxiPlayerController = toVC as? MaxiPlayerController else {fatalError()}
        
        guard let snapshot = fromVC.view.snapshotView(afterScreenUpdates: false) else { return }
        
        let fromRect = transitionContext.initialFrame(for: fromVC)
        var toRect = fromRect

        
        // Setup destionation View Controller in first position
        // At bottom of screen
        toRect.origin.y = initialY
        toVC.view.frame = toRect
        
        let container = transitionContext.containerView
        
        // Tabar
        let tabBarHeight = fromVC.tabBar.frame.height
        let tabBarImageView = UIImageView(frame: CGRect(x: 0, y: fromRect.height - tabBarHeight,
                                                        width: fromRect.width, height: tabBarHeight))
        tabBarImageView.image = fromVC.tabBar.makeSnapshot()
    
        // Background View
        let backgroundView = UIView(frame: fromRect)
        backgroundView.backgroundColor = .black
        
        // Title and button
        let miniController = fromVC.miniPlayerController
        miniController?.coverImageAvt.isHidden = true
        miniController?.visualEffectView.isHidden = true
        let miniView = miniController!.view.snapshotView(afterScreenUpdates: true)!
        maxiPlayerController.view.insertSubview(miniView, at: 0)
        
        let fakeView = UIView(frame: CGRect(x: 0, y: initialY, width: fromRect.width, height: miniView.frame.size.height))
        fakeView.backgroundColor = .white
        snapshot.addSubview(fakeView)
        
        container.addSubview(backgroundView)
        // Add to container
        container.addSubview(snapshot)
        container.addSubview(toVC.view)
        container.addSubview(tabBarImageView)
        
        let animOptions: UIView.AnimationOptions = [UIView.AnimationOptions.curveLinear, .allowAnimatedContent]//transitionContext.isInteractive ? [.curveLinear] : []
        
        let interval = transitionDuration(using: transitionContext)
        
        maxiPlayerController.configureImageLayerInStartPosition()
        maxiPlayerController.configureCoverImageInStartPosition()
        
        // Animation

        UIView.animate(withDuration: interval, delay: 0, options: animOptions, animations: {
            maxiPlayerController.animateImageLayerIn()
            maxiPlayerController.animateCoverImageIn()
            
            miniView.alpha = 0.0
            
            let dimmerAlpha: CGFloat = 0.3
            let cornerRadius: CGFloat = 15.0
            let aspectRatio: CGFloat = 15.0

            // Backing Image
            snapshot.frame = CGRect(x: aspectRatio, y: statusBarHeight(), width: fromRect.width - 2*aspectRatio, height: fromRect.height - 2*statusBarHeight())
            snapshot.alpha = dimmerAlpha
            snapshot.layer.cornerRadius = cornerRadius
            snapshot.layer.masksToBounds = true

            // TabBar
            tabBarImageView.center = CGPoint(x: tabBarImageView.center.x, y: tabBarImageView.center.y + tabBarHeight)

            maxiPlayerController.view.frame = CGRect(x: 0, y: statusBarHeight() + 10.0, width: fromRect.width, height: fromRect.height - statusBarHeight() + 10.0)

        }) { (finished) in

            miniController?.coverImageAvt.isHidden = false
            miniController?.visualEffectView.isHidden = false
            miniView.removeFromSuperview()
            
            fakeView.removeFromSuperview()
            
            tabBarImageView.removeFromSuperview()

            if transitionContext.transitionWasCancelled {
                
                backgroundView.removeFromSuperview()
                maxiPlayerController.view.removeFromSuperview()
                
                transitionContext.completeTransition(false)
            } else {
                transitionContext.completeTransition(true)
            }
        }
    }
    
    // Time
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return transitionContext!.isInteractive ? 0.4 : 0.4
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to) as? MaxiPlayerController else {return}
        
        animatePresentingInContext(transitionContext, fromVC: fromVC, toVC: toVC)
    }
}



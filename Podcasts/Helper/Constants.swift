//
//  Contents.swift
//  Podcasts
//
//  Created by Dai Long on 12/6/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import AVKit

var currentEpisode: Episode!

let systemVolume = AVAudioSession.sharedInstance().outputVolume

let userDefault = UserDefaults.standard
let podcastFavoriteKey = "podcastFavoriteKey"
let downloadEpisodeKey = "downloadEpisodeKey"

func statusBarHeight() -> CGFloat {
    let statusBarSize = UIApplication.shared.statusBarFrame.size
    return Swift.min(statusBarSize.width, statusBarSize.height)
}


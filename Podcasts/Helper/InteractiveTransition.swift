//
//  InteractiveTransition.swift
//  Podcasts
//
//  Created by Dai Long on 12/12/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

class InteractiveTransition: UIPercentDrivenInteractiveTransition {
    
    weak var viewController: UIViewController?
    weak var presentViewController: UIViewController?
    
    // PanGesture
    var panGesture: UIPanGestureRecognizer!
    
    var shouldComplete = false
    var lastProgress: CGFloat?
    
    init(viewController: UIViewController, presentVC: UIViewController? = nil) {
        super.init()
        self.viewController = viewController
        self.presentViewController = presentVC
        
        
        if let tabBar = viewController as? MainTabBarController {

            prepareGestureRecognizer(in: tabBar.containerView)
        }
        else if let vc = viewController as? MaxiPlayerController {
            prepareGestureRecognizer(in: vc.scrollView)
        }
    }
    
    private func prepareGestureRecognizer(in view: UIView) {
        self.panGesture = UIPanGestureRecognizer(target: self, action: #selector(onPan(pan:)))
        self.panGesture.delegate = self
        view.addGestureRecognizer(panGesture)
    }
    
    
    
    @objc private func onPan(pan: UIPanGestureRecognizer) {
        let translation = panGesture.translation(in: viewController?.view)
//        print("Translation y ", translation.y)
        // Represents the percentage of the transition that must be completed before allowing to complete.
        let threshold: CGFloat = 0.3
        
        let screenHeight = UIScreen.main.bounds.size.height - 50.0//BottomBar.bottomBarHeight
        let dragAmount: CGFloat = (presentViewController == nil) ? screenHeight : -screenHeight
        
        switch panGesture.state {
        case .began:
            print("began")
            if let presentViewController = presentViewController {
                viewController?.present(presentViewController, animated: true)
            }
            else {
                print(String(describing: viewController))
                viewController?.dismiss(animated: true)
            }
            
            
        case .changed:
            let velocity = panGesture.velocity(in: panGesture.view?.superview)
//            print("velocity: ", velocity)
            
            var progress: CGFloat = translation.y / dragAmount
            
            progress = fmax(progress, 0)
            progress = fmin(progress, 1)
            
//            print("progress, ", progress)

            // Updates the completion percentage of the transition.
            update(progress)

            shouldComplete = progress > threshold || velocity.y < -500.0
            
        case .ended, .cancelled:
            print("ended")
            if panGesture.state == .cancelled || shouldComplete == false {
                cancel()
            } else {
                finish()
            }
        default:
            break
        }
    }
}

extension InteractiveTransition: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let _ = viewController as? MaxiPlayerController else {return true}
        return false
    }
}

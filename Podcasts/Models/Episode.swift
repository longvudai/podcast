//
//  Episode.swift
//  Podcasts
//
//  Created by Dai Long on 11/22/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import FeedKit

struct Episode: Codable {
    let title: String!
    let author: String!
    let pubDate: Date!
    let description: String?
    var imageURL: String?
//    var artWork: UIImage?
    var streamUrl: String
    var fileUrl: String?
    
    init(_ feedItem: RSSFeedItem) {
        self.title = feedItem.title ?? ""
        self.author = feedItem.author ?? feedItem.iTunes?.iTunesAuthor
        self.pubDate = feedItem.pubDate ?? Date()
        self.description = feedItem.iTunes?.iTunesSummary ?? ""
        self.imageURL = feedItem.iTunes?.iTunesImage?.attributes?.href ?? ""
//        self.artWork =
        // Audio
        
        self.streamUrl = feedItem.enclosure?.attributes?.url ?? ""
    }
//
//    enum CodingKeys: String, CodingKey {
//        case title
//        case author
//        case pubDate
//        case description
//        case imageURL
//        case artWork
//        case streamUrl
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        container.encode
//    }
//
//    required init(from decoder: Decoder) throws {
//        <#code#>
//    }
}

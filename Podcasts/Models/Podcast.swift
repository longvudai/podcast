//
//  Podcast.swift
//  Podcasts
//
//  Created by Dai Long on 11/21/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation


class Podcast: NSObject, Codable, NSCoding {
    func encode(with aCoder: NSCoder) {
        print("encoding...")
        
        aCoder.encode(self.collectionName, forKey: "collectionNameKey")
        aCoder.encode(self.artistName, forKey: "artistNameKey")
        aCoder.encode(self.artworkUrl100, forKey: "artworkUrl100Key")
        aCoder.encode(self.feedUrl, forKey: "feedUrlKey")
    }
    
    required init?(coder aDecoder: NSCoder) {
        print("decoding...")
        self.collectionName = aDecoder.decodeObject(forKey: "collectionNameKey") as! String
        self.artistName = aDecoder.decodeObject(forKey: "artistNameKey") as? String
        self.artworkUrl100 = aDecoder.decodeObject(forKey: "artworkUrl100Key") as? String
        self.feedUrl = aDecoder.decodeObject(forKey: "feedUrlKey") as! String
    }
    
    var collectionName: String
    var artistName: String?
//    let artworkUrl60: String
    var artworkUrl100: String?
    var trackCount: Int = 0
    var feedUrl: String
    
    
}


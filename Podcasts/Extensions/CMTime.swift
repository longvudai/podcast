//
//  CMTime.swift
//  Podcasts
//
//  Created by Dai Long on 11/27/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import AVKit


extension CMTime {
    func toString() -> String {
        if self == CMTime.indefinite || self == CMTime.invalid {
            return "00:00"
        }
        let totalSeconds = Int(CMTimeGetSeconds(self))
        
        let minutes = totalSeconds / 60 % 60
        let seconds = totalSeconds % 60
        
        var strDuration = String(format:"%02d:%02d", minutes, seconds)
        
        if totalSeconds >= 3600 {
            let hours = totalSeconds/3600
            strDuration = String(format:"%02d:%02d:%02d", hours, minutes, seconds)
        }
        return strDuration
    }
}

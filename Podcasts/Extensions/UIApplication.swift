//
//  UIApplication.swift
//  Podcasts
//
//  Created by Dai Long on 12/4/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    
    static func mainTabBarController() -> MainTabBarController? {
        return shared.keyWindow?.rootViewController as? MainTabBarController
    }
    
    var screenShot: UIImage?  {
        
        if let layer = keyWindow?.layer {
            let scale = UIScreen.main.scale
            
            UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
            if let context = UIGraphicsGetCurrentContext() {
                layer.render(in: context)
                let screenshot = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                return screenshot
            }
        }
        return nil
    }
}


//extension UITabBarController {
//    open override var childForStatusBarStyle: UIViewController? {
//        print("TabBar ChildForStatusBarStyle")
//        return selectedViewController
//    }
//}
//
//extension UINavigationController {
//    open override var childForStatusBarStyle: UIViewController? {
//        print("Navigation ChildForStatusBarStyle")
//        return visibleViewController
//    }
//}


//
//  FavoritePodcastCell.swift
//  Podcasts
//
//  Created by Long Vu on 5/27/19.
//  Copyright © 2019 Dai Long. All rights reserved.
//

import UIKit

class FavoritePodcastCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    
    // MARK: - Properties
    var podcast: Podcast! {
        didSet {
            self.titleLabel.text = podcast.collectionName
            self.artistLabel.text = podcast.artistName
            self.imageView.sd_setImage(with: URL(string: podcast.artworkUrl100!), completed: nil)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        backgroundColor = .red
    }

}

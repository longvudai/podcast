//
//  EpisodeCell.swift
//  Podcasts
//
//  Created by Dai Long on 11/24/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

class EpisodeCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    // MARK: - Properties
    lazy var dateFormatter: DateFormatter = {
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy"
        return dateFormatter
    }()
    
    var episode: Episode! {
        didSet {
            self.dateLabel.text = self.dateFormatter.string(from: episode.pubDate)
            self.titleLabel.text = episode.title
            self.descriptionLabel.text = episode.description
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        progressView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

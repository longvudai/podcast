// This file contains the fastlane.tools configuration
// You can find the documentation at https://docs.fastlane.tools
//
// For a list of all available actions, check out
//
//     https://docs.fastlane.tools/actions
//

import Foundation

class Fastfile: LaneFile {
    
	func deployLane() {
        desc("Sent a build to TestFairy for our Beta testers")
		// add actions here: https://docs.fastlane.tools/actions
        gym(exportMethod: "development")
        testfairy(apiKey: "769c434ef5873f2b4e9a9bb3a5a21bf3082e3b5d")
	}
}
